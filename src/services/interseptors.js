import axios from 'axios'
import store from '@/state/store'
import { Errors } from '@/constants'

export const errorNotificationHandler = resp => {
    const statusText = resp.response && resp.response.statusText
    const errorMessage = statusText ? statusText : Errors.defaultMessage
    store.dispatch('notifications/setError', errorMessage)
}

export const applySimpleErrorNotificationHandler = axiosInstance => {
    axiosInstance.interceptors.response.use(
        response => response,
        async error => {
            errorNotificationHandler(error)
        }
    )
}

export default function applyInterceptors(axiosInstance) {
    axiosInstance.interceptors.response.use(
        response => response,
        async error => {
            errorNotificationHandler(error)

            if (error.response && error.response.status !== 401) {
                return new Promise((resolve, reject) => {
                    reject(error)
                })
            }

            try {
                await store.dispatch('auth/auth')

                const config = error.config
                config.headers.Authorization = `Bearer ${store.getters['auth/token']}`

                return new Promise((resolve, reject) => {
                    axios
                        .request(config)
                        .then(response => {
                            resolve(response)
                        })
                        .catch(error => {
                            reject(error)
                        })
                })
            } catch (err) {
                return new Promise((resolve, reject) => {
                    reject(error)
                })
            }
        }
    )

    // Add a request interceptor
    axiosInstance.interceptors.request.use(config => {
        config.headers.Authorization = `Bearer ${store.getters['auth/token']}`
        return config
    })

    return axiosInstance
}
