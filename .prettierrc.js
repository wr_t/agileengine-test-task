// https://github.com/vuejs/vue-cli/tree/dev/packages/%40vue/eslint-config-prettier

module.exports = {
    trailingComma: 'es5',
    jsxBracketSameLine: false,

    printWidth: 80, // https://prettier.io/docs/en/options.html#print-width
    tabWidth: 4, // https://prettier.io/docs/en/options.html#tab-width
    useTabs: false, // https://prettier.io/docs/en/options.html#tabs
    semi: false, // https://prettier.io/docs/en/options.html#semicolons
    singleQuote: true, // https://prettier.io/docs/en/options.html#quotes
    bracketSpacing: true, // https://prettier.io/docs/en/options.html#bracket-spacing
    arrowParens: 'avoid', // https://prettier.io/docs/en/options.html#arrow-function-parentheses
    proseWrap: 'never', // https://prettier.io/docs/en/options.html#prose-wrap
}
